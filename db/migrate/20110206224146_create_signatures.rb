class CreateSignatures < ActiveRecord::Migration
  def self.up
    create_table :signatures do |t|
      t.integer :user_id
      t.string :name
      t.text :content

      t.timestamps
    end
  end

  def self.down
    drop_table :signatures
  end
end
